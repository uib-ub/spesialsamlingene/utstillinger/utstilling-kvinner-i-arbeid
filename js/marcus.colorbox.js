$(document).ready(function(){
  //Examples of how to assign the ColorBox event to elements
  $("a[rel='example1']").colorbox();
  $("a[rel='example2']").colorbox({transition:"fade"});
  $("a[rel='example3']").colorbox({transition:"none", width:"75%", height:"75%"});
  $("a[rel='example4']").colorbox({slideshow:true});
  $(".single").colorbox({}, function(){
    alert('Howdy, this is an example callback.');
  });
  $(".colorbox").colorbox({photoScaling:true, maxwidth:"75%", maxheight:"75%", transition:"fade", current: "{current} av {total}", previous: "forrige", next:"neste", close:"lukk", opacity:0.85});
  $(".youtube").colorbox({iframe:true, width:650, height:550});
  $(".iframe").colorbox({width:"80%", height:"80%", iframe:true});
  $(".inline").colorbox({width:"50%", inline:true, href:"#inline_example1"});
  
  //Example of preserving a JavaScript event for inline calls.
  $("#click").click(function(){ 
    $('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
    return false;
  });
});
