# Kvinner i arbeid #

Dette repoet innholder nettutstillingen "Kvinner i arbeid", som før lå på Gyda. 

[Kvinner i arbeid](http://www.ub.uib.no/utstilling/kvinner-i-arbeid/index.html)

Utstillingen inneholder én artikkel av Ola Søndenå og én av Hanne Marie Johansen. Kodet av Tarje Lavik fra sommeren 2007.

Artiklene er delt opp i kapitler som har hver sin html-fil. Hvert kapittel har flere illustrasjoner. Illustrasjonene kan vises i større versjon ved hjelp av Colorbox.js.